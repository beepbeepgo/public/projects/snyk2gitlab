module.exports = {
  roots: ['<rootDir>'],
  //testRegex: '\\.spec\\.js$',
  transform: {
    '^.+\\.js?$': 'babel-jest',
  },
  moduleFileExtensions: ['js', 'json'],
  verbose: true,
  coverageThreshold: {
    global: {
      branches: 25,
      functions: 25,
      lines: 25,
      statements: -50
    }
  },
  coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
  reporters: ['default', 'jest-junit']
};
