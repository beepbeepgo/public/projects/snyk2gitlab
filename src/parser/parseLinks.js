function parseLinks(linksObj) {
  const linksSource = [];
  for (let i = 0; i < linksObj.length; i++) {
    linksSource.push(linksObj[i].url);
  }
  const linksObjTransform = linksSource.map(v => ({
      url: v,
    }));
  return linksObjTransform;
}
module.exports = parseLinks;
